# KlimTris [WIP]

Personal learning experiment to practise programming.

Libraries used: x11, gl, glu

Supports dvorak layout (controlling with arrows by default).

Score is shown only in the title.

## Controls

```
keyboard options:
	-q, --qwerty	set qwerty keyboard
	-d, --dvorak	set dvorak keyboard
	-a, --arrows	set arrows keyboard
Default settings for playing is arrows
Use 'q' ingame to exit the game
```

## Screenshot

![](https://k11m1.eu/imgs/klimTris-demo2.png)

